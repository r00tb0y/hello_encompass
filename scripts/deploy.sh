#!/bin/bash

###################################################################
# Title          : Gitlab CI deploy to Fargate
# Description    :
# Date           : Sun 17 Mar 20:48:28 WET 2019
# Author         : Moritz Schlichting
# Email          : 2718281(at)protonmail(dot)ch
###################################################################

apk add --update python python-dev py-pip
pip install awscli --upgrade

docker pull pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME

export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY

$(aws ecr get-login --no-include-email --region $AWS_REGION | tr -d '\r')

docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME $AWS_REGISTRY_IMAGE:$CI_ENVIRONMENT_SLUG
docker push $AWS_REGISTRY_IMAGE:$CI_ENVIRONMENT_SLUG

aws ecs register-task-definition --family helloflask-$CI_ENVIRONMENT_SLUG
--requires-compatibilities FARGATE --cpu 256 --memory 512 --cli-input-json
file://aws/helloflask-task-definition-$CI_ENVIRONMENT_SLUG.json --region $AWS_REGION

aws ecs update-service --cluster helloflask-$CI_ENVIRONMENT_SLUG --service
helloflask --task-definition helloflask-$CI_ENVIRONMENT_SLUG --region $AWS_REGION
