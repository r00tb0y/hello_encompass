FROM python:3.6

USER root

WORKDIR /app

RUN pip install --upgrade pip
RUN pip install pipenv
COPY ./requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY . /app/

EXPOSE 80

ENV NAME HelloWorld

CMD ["python", "app.py"]
