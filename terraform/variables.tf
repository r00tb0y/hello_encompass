variable "aws_region" {
    description = "Set geographic aws region"
    default     = "eu-west-2" # London, UK
}

variable "aws_zone_id" {
    description = "AWS zone ID"
    default     = "Z3GWWYV22T8SUV"
}

variable "domain" {
    description = "The actual static public domain for route53 routing"
    default     = "notmuch.space"
}

variable "ecs_task_execution_role_name" {
    description = "ECS task execution role name"
    default     = "myEcsTaskExecutionRole"
}

variable "ecs_auto_scale_role_name" {
    description = "ECS auto scale role name"
    default     = "myEcsAutoScaleRole"
}

variable "az_count" {
    description = "Number of AZs in region"
    default     = "2"
}

variable "app_image" {
    description = "The Docker image to be run on ECS"
    default     = "omnidexter/flaskhello:latest"
}

variable "app_port" {
    description = "The port exposedby docker image for traffic"
    default     = 80
}

variable "app_count" {
    description = "Number of containers to run"
    default     = 2
}

variable "health_check_path" {
    default = "/"
}

variable "fargate_cpu" {
    description = "Fargate CPU units with 1024=1CPU"
    default     = 1024
}

variable "fargate_memory" {
    description = "Memory of fargate instance in MB"
    default     = 2048
}
