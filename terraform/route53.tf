resource "aws_route53_delegation_set" "main" {
  reference_name = "DynDNS"
}

resource "aws_route53_zone" "primary_route" {
  name              = "${var.domain}"
  delegation_set_id = "${aws_route53_delegation_set.main.id}"
}

resource "aws_route53_record" "www" {
  zone_id = "${aws_route53_zone.primary_route.name}"
  name    = "www.${var.domain}"
  type    = "A"

  alias {
    name                    = "${aws_alb.main.name}"
    zone_id                 = "${aws_alb.main.zone_id}"
    evaluate_target_health  = true
  }
}
