## Encompass assignment

This repo contains a small Flask app returning "Hello World!" on all routes.
The app runs via Docker in a container and exposes port 80. The app can be
deployed to AWS Fargate with route53 DNS routing using terraform.

### Building the app

The app can be built with docker like so:

```bash
docker build -t helloflask .
```
which tags the created image as _helloflask_. To check the Flask app runs
correcty you can run it locally with:

```bash
docker run -p 80:80 helloflask
```
and navigate to the url shown in the console output (should be 0.0.0.0:80).

### Preparing AWS Docker registry
In order to run the containerized app on AWS using the Fargate infrastructure,
we need to correctly tag and upload the container image to Amazon's docker hub.
Make sure you have [installed](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) and [configured](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html) the AWS cli.

Create a new repository on AWS container registry (ECR) with:

```bash
aws ecr create-repository --repository-name flaskhello
```

Create a lifecycle policy for the containers e.g.:

```bash
aws ecr put-lifecycle-policy --registry-id <YourregistryID> --repository-name flaskhello --lifecycle-policy-text '{"rules":[{"rulePriority":10,"description":"Expire old images","selection":{"tagStatus":"any","countType":"imageCountMoreThan","countNumber":10},"action":{"type":"expire"}}]}'
```

You find the registry ID in the output of the previous command.

Now, login with your local docker daemon against AWS with:

```bash
$(aws ecr get-login --registry-ids <YouregisterID> --no-include-email)
```

Note: you can also run the above command without the surrounding $(...). That
will _only_ print the command you want to run to terminal. You will then have to
manually copy/past/run to achieve the desired effect.

### Preparing and uploading container

Next, we need to tag the our container appropriately for AWS. Since we have
named our container helloflask we don't need to use `docker images` to findthe
identifyer, but can run:

```bash
docker tag helloflask:latest <aws_account_id>.dkr.ecr.eu-west-2.amazonaws.com/flaskhello:latest
```

The aws\_account\_id should be in ~/.aws/credentials or whereever you have stored 
the credentials alternatively.

Now, we can finally upload the container to ECR with:

```bash
docker push <aws_account_id>.dkr.ecr.eu-west-2.amazonaws.com/flaskhello:latest
```

### Provisioning the app with terraform

Change your registered custom domain in `terraform/variables.tf` to match a
domain you own.

Make sure you have Terraform [installed and configured](https://learn.hashicorp.com/terraform/getting-started/install.html).

Now initialize terraform for the project with:

```bash
terraform init terraform
```

You can dry run/plan what terraform will do to check whether that matches your
expectations:

```bash
terraform plan terraform
```

If that is the case, you can execute the plan:

```bash
terraform apply terraform
```

which should apply the app into production on AWS fargate, point it to your
custom domain via route53 and therefore make it available via public internet on
that domain.

### GitLab CI

Under `Settings > CI / CD > Secret variables` add the appropriate

* AWS\_ACCESS\_KEY\_ID
* AWS\_REGION
* AWS\_REGISTRY\_IMAGE
* AWS\_SECRET\_ACCESS\_KEY

You may refer to the [AWS docs](https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html#access-keys-and-secret-access-keys)

In the `helloflask-task-definition-production.json` file adjust the
`executionRoleArn` to match your hosted zone ID if required. Also, adjust the
"image" accordingly to use the corresponding docker image id. Lastly, change the
`value` in `environment` to your route53 public url.

