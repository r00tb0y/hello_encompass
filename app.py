from flask import Flask


app = Flask(__name__)


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def home(path):
    try:
        return "Hello World!"
    except Exception as e:
        return str(e)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, ssl_context='adhoc')
